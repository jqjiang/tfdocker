FROM ubuntu:18.04

ARG USE_GPU=0
ARG TENSORFLOW_VERSION=1.15
ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /
COPY install.sh /install.sh
COPY env.sh /env.sh
RUN apt-get update && \
    apt-get install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev \
                       libnss3-dev libssl-dev libreadline-dev libffi-dev tk8.5-dev \
                       libbz2-dev liblzma-dev libsqlite3-dev uuid-dev \
                       libgdbm-compat-dev git curl wget unzip
RUN bash "/install.sh" && \
    rm -f "/install.sh"
RUN bash "/env.sh" && \
    rm -f "/env.sh"
