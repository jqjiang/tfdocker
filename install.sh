#!/usr/bin/env bash

set -e

export CUDA_COMPUTE_CAP=6.1,7.5
export CUDA_VERSION=10.0
export CUDNN_VERSION=7.6
export NCCL_VERSION=2.5
export CUDA_HOME="/usr/local/cuda-${CUDA_VERSION}"


install.python() {
    # Download and build
    echo "Installing Python"
    PYTHON_URL="https://npm.taobao.org/mirrors/python/3.7.6/Python-3.7.6.tgz"
    PYTHON_TMP_PATH="/tmp/python.tgz"
    PYTHON_TMP_DIR_PATH="/tmp/python"
    wget -nv "$PYTHON_URL" -O "$PYTHON_TMP_PATH"

    mkdir -p "$PYTHON_TMP_DIR_PATH"
    tar -xf "$PYTHON_TMP_PATH" -C "$PYTHON_TMP_DIR_PATH" --strip-components 1
    cd "$PYTHON_TMP_DIR_PATH"
    ./configure
    make -j $(nproc)
    make install
    ln -s /usr/local/bin/python3 /usr/bin/python
    ln -s /usr/local/bin/pip3 /usr/bin/pip 

    # Clean files
    cd /
    rm -f $PYTHON_TMP_PATH
    rm -rf $PYTHON_TMP_DIR_PATH
}

install.cuda() {
    # Install CUDA
    if [ "$CUDA_VERSION" = "10.0" ]; then
        CUDA_URL="https://developer.nvidia.com/compute/cuda/10.0/Prod/local_installers/cuda_10.0.130_410.48_linux"
        if [ "$CUDNN_VERSION" = "7.6" ]; then
            CUDNN_URL="https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1804/x86_64/libcudnn7_7.6.5.32-1+cuda10.0_amd64.deb"
            CUDNN_DEV_URL="https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1804/x86_64/libcudnn7-dev_7.6.5.32-1+cuda10.0_amd64.deb"
        else 
            echo "CUDA $CUDA_VERSION invalid for CuDNN $CUDNN_VERSION"
            return -1
        fi
        if [ "$NCCL_VERSION" = "2.5" ]; then
            NCCL_URL="https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1604/x86_64/libnccl2_2.5.6-1+cuda10.0_amd64.deb"
            NCCL_DEV_URL="https://developer.download.nvidia.cn/compute/machine-learning/repos/ubuntu1604/x86_64/libnccl-dev_2.5.6-1+cuda10.0_amd64.deb"
        else
            echo "CUDA $CUDA_VERSION invalid for NCCL $NCCL_VERSION"
            return -1
        fi
    else
        echo "CUDA version $CUDA_VERSION invalid"
        return -1
    fi

    CUDA_INSTALL_PATH="/tmp/cuda.run"

    CUDNN_TMP_PATH="/tmp/cudnn.deb"
    CUDNN_TMP_DIR_PATH="/tmp/cudnn"
    CUDNN_DEV_TMP_PATH="/tmp/cudnn-dev.deb"
    CUDNN_DEV_TMP_DIR_PATH="/tmp/cudnn-dev"

    NCCL_TMP_PATH="/tmp/nccl.deb"
    NCCL_TMP_DIR_PATH="/tmp/nccl"
    NCCL_DEV_TMP_PATH="/tmp/nccl-dev.deb"
    NCCL_DEV_TMP_DIR_PATH="/tmp/nccl-dev"

    echo "Downloading CUDA Libraries"
    wget -nv "$CUDA_URL" -O "$CUDA_INSTALL_PATH"
    wget -nv "$CUDNN_URL" -O "$CUDNN_TMP_PATH"
    wget -nv "$CUDNN_DEV_URL" -O "$CUDNN_DEV_TMP_PATH"
    wget -nv "$NCCL_URL" -O "$NCCL_TMP_PATH"
    wget -nv "$NCCL_DEV_URL" -O "$NCCL_DEV_TMP_PATH"
    
    # Install CUDA
    echo "Installing CUDA $CUDA_VERSION"
    bash "$CUDA_INSTALL_PATH" --silent --toolkit --override --toolkitpath="$CUDA_HOME"

    # Install CUDNN
    echo "Installing CuDNN $CUDNN_VERSION"
    mkdir -p "$CUDNN_TMP_DIR_PATH"
    mkdir -p "$CUDNN_DEV_TMP_DIR_PATH"

    cd "$CUDNN_TMP_DIR_PATH"
    ar x "$CUDNN_TMP_PATH"
    tar -xJf data.tar.xz
    cd "$CUDNN_DEV_TMP_DIR_PATH"
    ar x "$CUDNN_DEV_TMP_PATH"
    tar -xJf data.tar.xz

    mv $CUDNN_TMP_DIR_PATH/usr/lib/x86_64-linux-gnu/libcudnn* $CUDA_HOME/lib64/
    mv $CUDNN_DEV_TMP_DIR_PATH/usr/include/x86_64-linux-gnu/cudnn_v7.h $CUDA_HOME/include/
    mv $CUDNN_DEV_TMP_DIR_PATH/usr/lib/x86_64-linux-gnu/libcudnn_static_v7.a $CUDA_HOME/lib64/

    rm -f "$CUDA_HOME/include/cudnn.h"
    rm -f "$CUDA_HOME/lib64/libcudnn_static.a"

    ln -s "$CUDA_HOME/include/cudnn_v7.h" "$CUDA_HOME/include/cudnn.h"
    ln -s "$CUDA_HOME/lib64/libcudnn_static_v7.a" "$CUDA_HOME/lib64/libcudnn_static.a"

    # Instal NCCL
    echo "Installing NCCL $NCCL_VERSION"
    mkdir -p "$NCCL_TMP_DIR_PATH"
    mkdir -p "$NCCL_DEV_TMP_DIR_PATH"

    cd "$NCCL_TMP_DIR_PATH"
    ar x "$NCCL_TMP_PATH"
    tar -xJf data.tar.xz
    cd "$NCCL_DEV_TMP_DIR_PATH"
    ar x "$NCCL_DEV_TMP_PATH"
    tar -xJf data.tar.xz

    mv $NCCL_TMP_DIR_PATH/usr/lib/x86_64-linux-gnu/libnccl* "$CUDA_HOME/lib64/"
    rm -f "$CUDA_HOME/include/nccl.h"
    mv "$NCCL_DEV_TMP_DIR_PATH/usr/include/nccl.h" "$CUDA_HOME/include/nccl.h"
    rm -f "$CUDA_HOME/lib64/libnccl_static.a"
    mv "$NCCL_DEV_TMP_DIR_PATH/usr/lib/x86_64-linux-gnu/libnccl_static.a" "$CUDA_HOME/lib64/libnccl_static.a"

    # Setup Path & LD_LIBRARY_PATH
    echo "Configuring environment"
    export PATH="$CUDA_HOME/bin:${PATH}"
    export LD_LIBRARY_PATH="$CUDA_HOME/lib64:${LD_LIBRARY_PATH}"
    ldconfig

    # Clean files
    cd /
    rm -f $CUDA_INSTALL_PATH
    rm -f $CUDNN_TMP_PATH
    rm -f $CUDNN_DEV_TMP_PATH
    rm -f $NCCL_TMP_PATH
    rm -f $NCCL_DEV_TMP_PATH
    rm -rf $CUDNN_TMP_DIR_PATH
    rm -rf $CUDNN_DEV_TMP_DIR_PATH
    rm -rf $NCCL_TMP_DIR_PATH
    rm -rf $NCCL_DEV_TMP_DIR_PATH
    echo "Installed CUDA $CUDA_VERSION, CuDNN $CUDNN_VERSION, NCCL $NCCL_VERSION"
}

install.tensorflow() {
    pip3 config set global.index-url https://mirrors.aliyun.com/pypi/simple
    pip3 install six numpy wheel setuptools mock 'future>=0.17.1'
    pip3 install keras_applications --no-deps
    pip3 install keras_preprocessing --no-deps

    echo "Installing Bazel"
    BAZEL_URL="https://github.com/bazelbuild/bazel/releases/download/0.26.1/bazel-0.26.1-installer-linux-x86_64.sh"
    BAZEL_INSTALL_PATH="/tmp/bazel.sh"
    BAZEL_HOME="/tmp/bazel"
    wget -nv $BAZEL_URL -O $BAZEL_INSTALL_PATH
    bash $BAZEL_INSTALL_PATH --prefix=$BAZEL_HOME
    export PATH=$BAZEL_HOME/bin:$PATH
    
    echo "Installing Tensorflow $TENSORFLOW_VERSION"
    TF_ROOT="/tensorflow"
    rm -rf /tensorflow
    git clone "https://github.com/tensorflow/tensorflow.git" --depth 1 --branch "r$TENSORFLOW_VERSION"

    cd $TF_ROOT

    export PYTHON_BIN_PATH=$(which python3)
    export PYTHON_LIB_PATH="$($PYTHON_BIN_PATH -c 'import site; print(site.getsitepackages()[0])')"
    export PYTHONPATH=${TF_ROOT}/lib
    export PYTHON_ARG=${TF_ROOT}/lib

    # Compilation parameters
    export TF_NEED_CUDA=0
    export TF_NEED_GCP=0
    export TF_CUDA_COMPUTE_CAPABILITIES=$CUDA_COMPUTE_CAP
    export TF_NEED_HDFS=0
    export TF_NEED_OPENCL=0
    export TF_NEED_JEMALLOC=1
    export TF_ENABLE_XLA=1
    export TF_NEED_VERBS=0
    export TF_CUDA_CLANG=0
    export TF_DOWNLOAD_CLANG=0
    export TF_NEED_MKL=1
    export TF_DOWNLOAD_MKL=1
    export TF_NEED_MPI=0
    export TF_NEED_S3=0
    export TF_NEED_KAFKA=0
    export TF_NEED_GDR=0
    export TF_NEED_OPENCL_SYCL=0
    export TF_SET_ANDROID_WORKSPACE=0
    export TF_NEED_AWS=0
    export TF_NEED_IGNITE=0
    export TF_NEED_ROCM=0

    # Compiler options
    export GCC_HOST_COMPILER_PATH=$(which gcc)

    if [ "$USE_GPU" -eq "1" ]; then
        # Cuda parameters
        export TF_NEED_CUDA=1
        export TF_CUDA_VERSION=$CUDA_VERSION
        export TF_CUDNN_VERSION=$CUDNN_VERSION
        export TF_NEED_TENSORRT=0
        export TF_NCCL_VERSION=$NCCL_VERSION
    fi

    # Compilation
    ./configure

    if [ "$USE_GPU" -eq "1" ]; then
        bazel build --config=opt \
                    --config=cuda \
                    --linkopt="-lrt" \
                    --linkopt="-lm" \
                    --host_linkopt="-lrt" \
                    --host_linkopt="-lm" \
                    --action_env="LD_LIBRARY_PATH=${LD_LIBRARY_PATH}" \
                    //tensorflow/tools/pip_package:build_pip_package
        PACKAGE_NAME=tensorflow-gpu
    else
        bazel build --config=opt \
                    --linkopt="-lrt" \
                    --linkopt="-lm" \
                    --host_linkopt="-lrt" \
                    --host_linkopt="-lm" \
                    --action_env="LD_LIBRARY_PATH=${LD_LIBRARY_PATH}" \
                    //tensorflow/tools/pip_package:build_pip_package
        PACKAGE_NAME=tensorflow
    fi

    bazel-bin/tensorflow/tools/pip_package/build_pip_package "/tmp/tensorflow" --project_name "$PACKAGE_NAME"

    pip3 install /tmp/tensorflow/tensorflow*.whl

    cd /
    rm -rf $TF_ROOT
    rm -rf "/tmp/tensorflow/"
    rm -f $BAZEL_INSTALL_PATH
    rm -rf $BAZEL_HOME
}

install.clean() {
    rm -rf /tmp
    rm -rf /root/.cache
}


if [ "$USE_GPU" -eq "1" ]; then
    install.cuda
fi
install.python
install.tensorflow
install.clean