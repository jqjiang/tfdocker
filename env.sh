#!/usr/bin/env bash

set -e

env.nlukernel() {
    pip install numpy==1.17 pandas==0.25 scikit_learn==0.22 keras==2.3
    pip install git+https://www.github.com/keras-team/keras-contrib.git#egg=keras-contrib

}

env.clean() {
    rm -rf /root/.cache
}

env.nlukernel
env.clean